
<h1 style="display:block;text-align:center"><img src="https://gitlab.com/GIS-projects/Plugin-Load-Times/raw/master/PluginLoadTimes/icon.png"> Plugin Load Times for <a href="http://qgis.org" target="_blank">QGIS 3</a></h1>

If QGIS takes long to start, it can be because some of the plugins you installed take a long time to load at startup.  This plugin shows the load time of each plugin. It does not give you information about the speed of plugins when they are used. With the information provided by this plugin you can easily decide which plugins to disable, to make QGIS start faster.

This plugin adds a toolbar button and an item to the plugin menu to see how long each plugin loads in QGIS 3. It shows data from the *QGIS application runtime profiler* in an easy to understand window. It's possible to sort the results based on plugin name or on load time. It also has a pie chart with the data visualized.

For Plugin Load Times v.4 and up, the minimum required QGIS version is 3.16.

&nbsp;

&nbsp;

<span style="display:block;text-align:center; margin-top: 3em;"><b>If you use the plugin and notice a problem with it, please <a href="https://gitlab.com/GIS-projects/Plugin-Load-Times/-/issues">report a bug</a>.</b></span>

&nbsp;

<span style="display:block;text-align:center"><b><a href="https://plugins.qgis.org/plugins/PluginLoadTimes/">This plugin is available in the official QGIS Plugin Repository, so it can be installed from within QGIS.</a></b></span>&nbsp;

<span style="display:block;text-align:center"><b><a href="https://gitlab.com/GIS-projects/Plugin-Load-Times/-/releases">You can also download the latest stable version from Gitlab and install it manually.</a></b></span>

&nbsp;

<span style="display:block;text-align:center">This plugin was created by <a href="https://stuyts.xyz" target="_blank">Michel Stuyts</a>.</span>
