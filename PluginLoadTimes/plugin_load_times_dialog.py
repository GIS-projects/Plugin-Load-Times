# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginLoadTimesDialog
                                 A QGIS plugin
 Show how long each QGIS plugin loads
                             -------------------
        begin                : 2017-01-18
        copyright            : (C) 2017 by Michel Stuyts
        email                : info@stuyts.xyz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt import uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'plugin_load_times_dialog_base.ui'))


class PluginLoadTimesDialog(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        super(PluginLoadTimesDialog, self).__init__(parent)
        self.resize(QSize(700, 500).expandedTo(self.minimumSizeHint()))
        self.setWindowIcon(QIcon(":/plugins/PluginLoadTimes/icon.png"))
        try:
            self.setWindowFlags( self.windowFlags() & ~Qt.WindowContextHelpButtonHint | Qt.WindowMinMaxButtonsHint)
        except AttributeError:
            self.setWindowFlags( self.windowFlags() & ~Qt.WindowType.WindowContextHelpButtonHint | Qt.WindowType.WindowMinMaxButtonsHint)
        self.setupUi(self)
