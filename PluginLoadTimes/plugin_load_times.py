# -*- coding: utf-8 -*-
"""
/***************************************************************************
 PluginLoadTimes: A QGIS plugin that shows how long each QGIS plugin loads
                              -------------------
        begin                : 2017-01-18
		latest update		 : 2023-09-28
        made by              : Michel Stuyts
        email                : info@stuyts.xyz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.PyQt.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from qgis.PyQt.QtGui import QIcon, QColor, QPen, QFont
from qgis.PyQt.QtWidgets import QAction, QGraphicsScene, QGraphicsView, QGraphicsEllipseItem,QGraphicsRectItem,QGraphicsTextItem
from qgis.core import QgsApplication
import qgis.utils
from .resources3 import *
from .plugin_load_times_dialog import PluginLoadTimesDialog
import os.path
import sys, random, json

class PluginLoadTimes:
    def __init__(self, iface):
        """Constructor.
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        
        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Plugin Load Times')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'PluginLoadTimes')
        self.toolbar.setObjectName(u'PluginLoadTimes')
        # Create the dialog (after translation) and keep reference
        self.dlg = PluginLoadTimesDialog()
        self.dlg.sortspeed.clicked.connect(self.sortingspeed)
        self.dlg.sortspeedrev.clicked.connect(self.sortingspeedrev)
        self.dlg.sortalphabetical.clicked.connect(self.sortingalphabetical)
        self.dlg.sortalphabeticalrev.clicked.connect(self.sortingalphabeticalrev)

    def getplugindetails(self):
        # get names of plugins
        global pluginname
        global pluginversion
        pluginname = {}
        pluginversion = {}
        for path in qgis.utils.plugin_paths:
            for pluginrange in qgis.utils.findPlugins(path):
                for key in pluginrange[1].options('general'):
                    if key=="name":
                        pluginname[pluginrange[0]]=pluginrange[1].get('general',key)
                    if key=="version":
                        pluginversion[pluginrange[0]]=pluginrange[1].get('general',key)
        source_data_variable=json.dumps(self.getprofilerdata())
        self.dlg.source_data.setText('<span style="color: #777777">'+source_data_variable+'</span>')

    def getprofilerdata(self):
        profiler = QgsApplication.profiler()
        plugins = {}
        
        for profiler_row in range(0, profiler.rowCount()):
            profiler_data = []
            for profiler_col in range(0, profiler.columnCount()):
                index = profiler.index(profiler_row, profiler_col)
                profiler_data.append(profiler.data(index))
        
            if(profiler_data[0]==QCoreApplication.translate("QObject", "Load plugins")):
                total_time = profiler_data[1]
                for plugin_row in range(0, profiler.rowCount(index)):
                    for plugin_col in range(0, profiler.columnCount(index)):
                        plugin_information = profiler.data(profiler.index(plugin_row, plugin_col, index))
                        if(plugin_col==0):
                            plugin_systemname = plugin_information
                        else:
                            plugin_loadtime = plugin_information
                            plugins[plugin_systemname]=plugin_loadtime

        return plugins

    def colorcode(self,time):
        if float(time)<0.1:
            color="green"
        elif float(time)<1:
            color="#eedf00"
        elif float(time)<5:
            color="orange"
        else:
            color="red"
        return color

    def tr(self, message):
        return QCoreApplication.translate('PluginLoadTimes', message)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)
        if status_tip is not None:
            action.setStatusTip(status_tip)
        if whats_this is not None:
            action.setWhatsThis(whats_this)
        if add_to_toolbar:
            self.toolbar.addAction(action)
        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)
        self.actions.append(action)
        return action

    def addgraph(self):
        data = self.getprofilerdata()
        self.getplugindetails()
        scene = QGraphicsScene()
        set_angle = 0
        small_times = 0
        small_name="all other plugins"
        small_colour=QColor(77,77,77)
        colours = [QColor(93,165,218),QColor(250,164,58),QColor(96,189,104),QColor(241,124,176),QColor(178,145,47),QColor(178,118,178),QColor(222,207,63),QColor(241,88,84)]
        countcolors = 0
        plugins = []
        times = []
        percentage = []
        for key,value in sorted(data.items(), key=lambda x: x[1], reverse=True):
            number = []
            if key in pluginname:
                plugins.append(pluginname[key]+ " " +pluginversion[key])
                times.append(float(value))
                countcolors += 1
                if countcolors>8:
                    for count in range(3):
                        number.append(random.randrange(0, 255))
                    colours.append(QColor(number[0],number[1],number[2]))
        totaltime=round(sum(times),6)
        numberoftimes=len(times)
        listrange=range(0, numberoftimes-1)
        for id in listrange:
            percentage.append(times[id]/totaltime)
            # only plugins which take more than 2% of the total load time are shown in the pie chart. All other plugins go into the category "all other plugins".
            if percentage[id]>0.02:
                angle = round(float(times[id]*5760)/totaltime)
                ellipse = QGraphicsEllipseItem(0,0,200,200)
                ellipse.setPos(100,0)
                ellipse.setStartAngle(set_angle)
                ellipse.setSpanAngle(5760-set_angle)
                ellipse.setBrush(colours[id])
                ellipse.setPen(colours[id])
                set_angle += angle
                scene.addItem(ellipse)
                rectangle=QGraphicsRectItem(-270,(id*30)+25,10,10)
                rectangle.setBrush(colours[id])
                rectangle.setPen(colours[id])
                scene.addItem(rectangle)
                if len(plugins[id])>35:
                    plugins[id]=plugins[id][:32]+'...'
                legend=QGraphicsTextItem(plugins[id]+" ("+str(float(round(percentage[id]*100,1)))+"%)")
                legend.setPos(-255,(id*30)+20)
                legend.setFont(QFont('Courier',8))
                scene.addItem(legend)
                maxid=id
                
            else:
                small_times+=times[id]
        if small_times>0:
            angle = 5760-set_angle
            ellipse = QGraphicsEllipseItem(0,0,200,200)
            ellipse.setPos(100,0)
            ellipse.setStartAngle(set_angle)
            ellipse.setSpanAngle(angle)
            ellipse.setBrush(small_colour)
            ellipse.setPen(small_colour)
            scene.addItem(ellipse)   
            rectangle=QGraphicsRectItem(-270,((maxid+1)*30)+25,10,10)
            rectangle.setBrush(small_colour)
            rectangle.setPen(small_colour)
            scene.addItem(rectangle)    
            legend=QGraphicsTextItem(small_name)
            legend.setDefaultTextColor(QColor(160,160,160))
            legend.setPos(-255,((maxid+1)*30)+20)
            legend.setFont(QFont('Courier',8))
            scene.addItem(legend)
        self.dlg.graphicsView.setScene(scene)
        self.dlg.show()
        
    def sortingplugins(self, sorting):
        if sorting=='speed':
            varkey=lambda x: float(x[1]);
            varreverse=False;
        elif sorting=='speedrev':
            varkey=lambda x: float(x[1]);
            varreverse=True;
        elif sorting=='alphabetical':
            varkey=lambda x: x[0].lower();
            varreverse=False;
        elif sorting=='alphabeticalrev':
            varkey=lambda x: x[0].lower();
            varreverse=True;
        else:
            pass;
        data = self.getprofilerdata()
        self.getplugindetails()
        tabletotal=0
        outputtext="<table style='border: none;'>"
        for key,value in sorted(data.items(), key=varkey, reverse=varreverse):
            if key in pluginname:
                color=self.colorcode(value)
                outputtext += "<tr style='font-weight: bold; font-family: tahoma, arial; font-size: 11pt; color: " + color  + ";'><td style='padding-bottom: 0.5em;'><span style='font-family: \"Courier New\", Courier, monospace'>" +key +":</span></td><td>" + pluginname[key] + " " + pluginversion[key] + ":</td><td style='padding-bottom: 0.5em;'>" + str(value) + " sec.</td></tr>"
        outputtext += "</table>"
        if len(data)==0:
            outputtext += "I'm sorry there is no data here. Apparantly this plugin doesn't work yet in this language version of QGIS.  Please inform the developer of this plugin about this problem by submitting a bug report.  This can be done at <strong>https://gitlab.com/GIS-projects/Plugin-Load-Times/-/issues</strong> . Be sure to mention your QGIS version and the language you use QGIS in, in the bug report."
        self.dlg.showloadtimes.setText(outputtext)
        self.dlg.show()

    def sortingspeed(self):
        self.sortingplugins('speed')

    def sortingspeedrev(self):
        self.sortingplugins('speedrev')

    def sortingalphabetical(self):
        self.sortingplugins('alphabetical')

    def sortingalphabeticalrev(self):
        self.sortingplugins('alphabeticalrev')

    def initGui(self):
        icon_path = ':/plugins/PluginLoadTimes/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Plugin Load Times'),
            callback=self.run,
            parent=self.iface.mainWindow())

    def unload(self):
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Plugin Load Times'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def run(self):
        # show the results
        self.sortingspeedrev()
        self.addgraph()
        # Run the dialog event loop
        try:
            result = self.dlg.exec_()
        except AttributeError:
            result = self.dlg.exec()
        # See if OK was pressed
        if result:
            pass

